package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Estado;
import com.example.BlogRestaurante.repositories.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstadoServiceImpl implements EstadoService{

    @Autowired
    EstadoRepository estadoRepository;

    @Override
    public Iterable<Estado> listAllEstados() {
        return estadoRepository.findAll();
    }

    @Override
    public Estado getEstadoById(Integer id) {
        return estadoRepository.findById(id);
    }

    @Override
    public Estado saveEstado(Estado option) {
        return estadoRepository.save(option);
    }

    @Override
    public void deleteEstado(Integer id) { estadoRepository.delete(id); }




}

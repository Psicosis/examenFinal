package com.example.BlogRestaurante.services;
import com.example.BlogRestaurante.entities.Pais;

/**
 * Created by July on 12/12/2017.
 */


public interface PaisService {

    Iterable<Pais> listAllPaises();
    Pais getPaisById(Integer id);
    Pais savePais(Pais pais);
    void deletePais(Integer id);
    void savePaisEdited(Pais pais);
}

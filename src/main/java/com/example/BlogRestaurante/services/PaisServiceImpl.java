package com.example.BlogRestaurante.services;

import org.springframework.stereotype.Service;
import com.example.BlogRestaurante.entities.Pais;
import com.example.BlogRestaurante.repositories.PaisRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by July on 12/12/2017.
 */
@Service
public class PaisServiceImpl implements PaisService {

    @Autowired
    PaisRepository paisRepository;

    @Override
    public Iterable<Pais> listAllPaises() {
        return paisRepository.findAll();
    }

    @Override
    public Pais getPaisById(Integer id) {
        return paisRepository.findById(id);
    }

    @Override
    public Pais savePais(Pais pais) {
        return paisRepository.save(pais);
    }

    @Override
    public void deletePais(Integer id) {
        paisRepository.delete(id);
    }

    @Override
    public void savePaisEdited(Pais pais) {
        paisRepository.save(pais);
    }

}

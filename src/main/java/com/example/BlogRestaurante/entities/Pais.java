package com.example.BlogRestaurante.entities;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * Created by July on 12/12/2017.
 */

@Entity
@Table(name = "pais")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "nombrePais")
    @Length(min = 0,max = 50)
    private String nombre_pais;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_pais() {
        return nombre_pais;
    }

    public void setNombre_pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
    }
}

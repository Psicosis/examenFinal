package com.example.BlogRestaurante.controllers;

import org.springframework.stereotype.Controller;
import com.example.BlogRestaurante.entities.Pais;
import com.example.BlogRestaurante.services.PaisService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;

/**
 * Created by July on 12/12/2017.
 */
@Controller
public class PaisController {

    private PaisService paisService;

    @Autowired
    public void setPaisService(PaisService paisService) {
        this.paisService= paisService;
    }


    @RequestMapping(value = "/admin/pais",  method = RequestMethod.GET)
    public String listRestaurants(Model model) {
        model.addAttribute("paises", paisService.listAllPaises());
        return "paisLista";
    }

    @RequestMapping(value = "/admin/pais/new", method = RequestMethod.GET)
    public String newPais(Model model) {
        model.addAttribute("pais", new Pais());

        return "paisNewFormulario";
    }


    @RequestMapping(value = "/admin/pais/edited", method = RequestMethod.POST)
    public String savePaisEdited(@Valid Pais pais, BindingResult bindingResult,Model model) {
        if(bindingResult.hasErrors()){
            return "paisNewFormulario";
        }
        paisService.savePaisEdited(pais);
        return "redirect:/admin/paises";
    }


    @RequestMapping(value = "/admin/paises",  method = RequestMethod.GET)
    public String listPaises(Model model) {
        model.addAttribute("paises", paisService.listAllPaises());
        return "paisLista";
    }

    @RequestMapping(value = "/admin/pais/{id}", method = RequestMethod.GET)
    public String showPais(@PathVariable Integer id, Model model) {
        Pais pais = paisService.getPaisById(id);
        model.addAttribute("pais", pais);
        return "pais";
    }

    @RequestMapping(value = "/admin/pais/delete/{id}", method = RequestMethod.GET)
    public String deletePais(@PathVariable Integer id) {
        paisService.deletePais(id);
        return "redirect:/admin/paises";
    }


}

package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface EstadoRepository extends JpaRepository <Estado, Integer> {


    Estado findById(Integer id);
}

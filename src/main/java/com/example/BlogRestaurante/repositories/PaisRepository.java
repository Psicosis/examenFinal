package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.Pais;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by July on 12/12/2017.
 */
@Transactional
public interface PaisRepository extends JpaRepository<Pais,Integer> {
    Pais findById(Integer id);
}
